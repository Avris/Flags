## Avris Flags ##

Check out [flags.avris.it](https://flags.avris.it) for usage instructions.

To build the CSS + PNG yourself:

 - `./flags flags:scrape <height in pixels> <countries comma separated>`
 - `./flags flags:generate <height in pixels> <countries comma separated>`

### Copyright ###

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
