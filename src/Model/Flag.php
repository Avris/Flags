<?php
namespace Avris\Flags\Model;

final class Flag
{
    const RATIO = 1.6;

    /** @var string */
    protected $code;

    /** @var int */
    protected $x;

    /** @var int */
    protected $y;

    /**
     * @param string $code
     * @param int $x
     * @param int $y
     */
    public function __construct($code, $x, $y)
    {
        $this->code = $code;
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }
}