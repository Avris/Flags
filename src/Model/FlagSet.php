<?php
namespace Avris\Flags\Model;

use Imagine\Image\ImageInterface;

final class FlagSet
{
    /** @var Flag[] */
    private $flags = [];

    /** @var int */
    private $width = 0;

    /** @var int */
    private $height = 0;

    /** @var ImageInterface */
    private $image;

    /** @return Flag[] */
    public function getFlags()
    {
        return $this->flags;
    }

    public function addFlag(Flag $flag)
    {
        $this->flags[$flag->getCode()] = $flag;
        $this->width = max($this->width, $flag->getX() + 1);
        $this->height = max($this->height, $flag->getY() + 1);
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getImage(): ImageInterface
    {
        return $this->image;
    }

    public function setImage(ImageInterface $image): self
    {
        $this->image = $image;
        return $this;
    }
}
