<?php
namespace Avris\Flags\Service;

interface ScraperInterface
{
    public function scrape(int $size = 64, array $countries = []): iterable;
}
