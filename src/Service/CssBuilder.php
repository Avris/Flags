<?php
namespace Avris\Flags\Service;

use Avris\Flags\Model\Flag;
use Avris\Flags\Model\FlagSet;

final class CssBuilder
{
    public function build(FlagSet $flagSet, float $border = 0.00): string
    {
        $borderedWidth = Flag::RATIO + 2 * $border;
        $borderedHeight = 1 + 2 * $border;

        $canvasWidth = $flagSet->getWidth() * $borderedWidth;
        $canvasHeight = $flagSet->getHeight() * $borderedHeight;
        $canvasHeightWhole = 2 * $canvasHeight;

        $css = "[class^='flag-'],[class*=' flag-'] {
            background-image: url(avris-flags.png);
            background-repeat: no-repeat;
            background-size: {$canvasWidth}em {$canvasHeightWhole}em;
            display: inline-block;
            vertical-align: middle;
            overflow: hidden;
            width: 1.6em;
            height: 1em;
        }
        .flag-circle {
            border-radius: 50%;
        }
        ";

        foreach(['xs' => '0.5em', 'sm' => '0.75em', 'lg' => '1.25em', 'xl' => '1.5em'] as $size => $scale) {
            $css .= ".text-$size {font-size: {$scale};vertical-align: middle;display: inline-block;height: 1em;} .text-{$size} > * {vertical-align: super;}";
        }

        $centerShift = (Flag::RATIO - 1) / 2 + $border;
        foreach ($flagSet->getFlags() as $flag) {
            $x = $borderedWidth * $flag->getX() + $border;
            $y = $borderedHeight * $flag->getY() + $border;
            $css .= ".flag-{$flag->getCode()} {background-position: -{$x}em -{$y}em;}";

            $y += $canvasHeight;
            $css .= ".flag-{$flag->getCode()}.flag-outbound {background-position: -{$x}em -{$y}em;}";

            $centerX = $x + $centerShift;
            $css .= ".flag-{$flag->getCode()}.flag-square, .flag-{$flag->getCode()}.flag-circle {background-position: -{$centerX}em -{$y}em;width: 1em;}";
        }

        return $css;
    }
}
