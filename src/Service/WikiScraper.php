<?php
namespace Avris\Flags\Service;

use Symfony\Component\Filesystem\Filesystem;

final class WikiScraper implements ScraperInterface
{
    private const SOURCES = [
        'https://commons.wikimedia.org/wiki/Sovereign-state_flags',
        'https://commons.wikimedia.org/wiki/Dependent_territory_flags',
        'https://commons.wikimedia.org/wiki/Geographical_flags',
    ];

    private const EXCEPTIONS = [
        'people\'s republic of china' => 'CN',
        'democratic republic of congo' => 'CD',
        'republic of congo' => 'CG',
        'myanmar' => 'MM',
        'sao tome & principe' => 'ST',
        'east timor' => 'TL',
        'burma' => 'MM',
        'republic of china' => 'TW',
        'hong kong' => 'HK',
        'macau' => 'MO',
    ];

    private const SINGLES = [
        'EU' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Flag_of_Europe.svg/255px-Flag_of_Europe.svg.png',
        'PS' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/00/Flag_of_Palestine.svg/255px-Flag_of_Palestine.svg.png',
        'CI' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Flag_of_C%C3%B4te_d%27Ivoire.svg/110px-Flag_of_C%C3%B4te_d%27Ivoire.svg.png',
        'FM' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e4/Flag_of_the_Federated_States_of_Micronesia.svg/125px-Flag_of_the_Federated_States_of_Micronesia.svg.png',
    ];

    /** @var Filesystem */
    private $filesystem;

    /** @var string */
    private $dir;

    public function __construct(Filesystem $filesystem, string $envFlagsDir)
    {
        $this->filesystem = $filesystem;
        $this->dir = $envFlagsDir . '/';
    }

    public function scrape(int $size = 64, array $countries = []): iterable
    {
        yield sprintf('<info>Saving to "%s"</info>', $this->dir);
        $this->filesystem->remove($this->dir);
        $this->filesystem->mkdir($this->dir);

        $fetched = [];

        foreach (self::SOURCES as $source) {
            yield 'SCRAPE ' . $source;
            foreach ($this->scrapeFlags($source, $size, $countries) as $code => $src) {
                if (array_key_exists($code, $countries)) {
                    $this->saveImage($src, $code);
                    yield sprintf('SAVED %s', $code);
                    $fetched[] = $code;
                }
            }
        }

        foreach (self::SINGLES as $code => $url) {
            if (array_key_exists($code, $countries)) {
                $src = preg_replace('#\/\d+px#', '/'.$size.'px', $url);
                $this->saveImage($src, $code);
                yield sprintf('SAVED %s', $code);
                $fetched[] = $code;
            }
        }

        $fetched = array_unique(array_filter($fetched));

        yield sprintf(
            '<info>FETCHED %s/%s, %s missing</info>',
            count($fetched),
            count($countries),
            count($countries) - count($fetched)
        );

        $missing = array_diff(array_keys($countries), $fetched);

        foreach ($missing as $code) {
            yield sprintf(' - %s ("%s")', $code, $countries[$code]);
        }
    }

    private function scrapeFlags(string $url, int $size, array $countries)
    {
        $html = file_get_contents($url);
        $dom = new \DOMDocument;
        $dom->loadHTML($html);
        $dom->preserveWhiteSpace = false;
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $image) {
            yield from $this->handleImage($image, $size, $countries);
        }
    }

    private function handleImage(\DOMElement $image, int $size, array $countries): iterable
    {
        $src = $image->getAttribute('src');

        if (substr($src, 0, 1) == '/' || substr($src, -4) !== '.png') {
            return;
        }

        $src = preg_replace('#\/\d+px#', '/' . $size.'px', $src);

        $name = $image->getAttribute('alt')
            ?: $image->parentNode->parentNode->parentNode->parentNode->nodeValue;

        $name = preg_replace('#\([^)]*\)#', '', $name);
        $name = preg_replace('#,.*\)#', '', $name);
        $name = strtolower(trim($name));

        $name = preg_replace('# and #', ' & ', $name);
        $name = preg_replace('#^flag of #', '', $name);
        $name = preg_replace('#^the #', '', $name);
        $name = preg_replace('# the #', ' ', $name);
        $name = preg_replace('#^saint\W#', 'st. ', $name);

        if (isset(self::EXCEPTIONS[$name])) {
            yield self::EXCEPTIONS[$name] => $src;
            return;
        }

        if (substr($name, 3) == 'te d\'ivoire') {
            yield 'CI' => $src;
            return;
        }

        foreach ($countries as $code => $cname) {
            if (strtolower($cname) === $name) {
                yield $code => $src;
            }
        }
    }

    private function saveImage(string $src, string $code): ?string
    {
        $path = $this->dir . $code . '.png';

        if (file_exists($path)) {
            return null;
        }

        file_put_contents($this->dir . $code . '.png', file_get_contents($src));

        return $code;
    }
}
