<?php
namespace Avris\Flags\Service;

final class CssMinifier
{
    public function minify(string $css): string
    {
        // WARNING: works only for the specific CSS, it's too simple to be generic
        $css = preg_replace('#\s*([{};:])\s*#m', '$1', $css);
        $css = preg_replace('#;}#m', '}', $css);

        return $css;
    }
}
