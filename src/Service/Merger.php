<?php
namespace Avris\Flags\Service;

use Avris\Flags\Model\Flag;
use Avris\Flags\Model\FlagSet;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Image\ManipulatorInterface;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Symfony\Component\Filesystem\Filesystem;

final class Merger
{
    const MAX_WIDTH = 16;

    /** @var ImagineInterface */
    private $imagine;

    /** @var Filesystem */
    private $filesystem;

    /** @var string */
    private $flagsDir;

    public function __construct(
        ImagineInterface $imagine,
        Filesystem $filesystem,
        string $envFlagsDir
    )
    {
        $this->imagine = $imagine;
        $this->filesystem = $filesystem;
        $this->flagsDir = $envFlagsDir;
    }

    /**
     * @param array $countries [ string $code => true | string $customFile ]
     */
    public function merge(int $height, array $countries = [], float $border = 0.0): FlagSet
    {
        $width = $height * Flag::RATIO;
        $borderedWidth = $height * (Flag::RATIO + 2 * $border);
        $borderedHeight = $height * (1 + 2 * $border);

        /** @var ImageInterface[] $images */
        $images = [
            ManipulatorInterface::THUMBNAIL_INSET => $this->imagine->create(
                new Box(self::MAX_WIDTH * $borderedWidth + 1, self::MAX_WIDTH * $borderedHeight + 1),
                (new RGB())->color('#000', 0)
            ),
            ManipulatorInterface::THUMBNAIL_OUTBOUND => $this->imagine->create(
                new Box(self::MAX_WIDTH * $borderedWidth + 1, self::MAX_WIDTH * $borderedHeight + 1),
                (new RGB())->color('#000', 0)
            ),
        ];

        $set = new FlagSet();
        $x = 1;
        $y = 0;

        foreach ($countries as $code => $file) {
            $flagFilename = sprintf(
                '%s/%s',
                $this->flagsDir,
                $file === true ? $code . '.png' : $file
            );

            foreach ($images as $mode => $image) {
                $flag = $this->imagine->open($flagFilename)
                    ->thumbnail(new Box($width, $height), $mode);

                $x1 = floor(($width - $flag->getSize()->getWidth()) / 2);
                $y1 = floor(($height - $flag->getSize()->getHeight()) / 2);

                $image->paste(
                    $flag,
                    new Point(
                        $x * $borderedWidth + $x1 + $border * $width,
                        $y * $borderedHeight + $y1 + $border * $width
                    )
                );
            }

            $set->addFlag(new Flag($code, $x, $y));

            if ($x + 1 >= self::MAX_WIDTH) {
                $x = 0;
                $y++;
            } else {
                $x++;
            }
        }

        $croppedSize = new Box(
            $set->getWidth() * $borderedWidth,
            $set->getHeight() * $borderedHeight
        );

        $singleImage = $this->imagine->create(
            new Box(
                $croppedSize->getWidth(),
                count($images) * $croppedSize->getHeight()
            ),
            (new RGB())->color('#000', 0)
        );

        $i = 0;
        foreach ($images as $mode => $image) {
            $image->crop(new Point(0, 0), $croppedSize);
            $singleImage->paste($image, new Point(0, $i++ * $croppedSize->getHeight()));
        }

        $set->setImage($singleImage);

        return $set;
    }
}
