<?php
namespace Avris\Flags\Service;

use Symfony\Component\Filesystem\Filesystem;

final class Generator
{
    const NPM_PACKAGE = [
        'name' => 'avris-flags',
        'version' => '2.0.0',
        'description' => 'Flags on websites made easy!',
        'keywords' => ['flag'],
        'homepage' => 'https://flags.avris.it',
        'author' => 'Andre Prusinowski <andre@avris.it>',
        'repository' => [
            'type' => 'git',
            'url' => 'https://gitlab.com/Avris/Flags.git'
        ],
        'license' => 'MIT',
    ];

    /** @var Filesystem */
    private $filesystem;

    /** @var string */
    private $flagsDir;

    /** @var string */
    private $distDir;

    /** @var Merger */
    private $merger;

    /** @var CssBuilder */
    private $cssBuilder;

    /** @var CssMinifier */
    private $cssMinifier;

    public function __construct(
        Filesystem $filesystem,
        string $envFlagsDir,
        string $envDistDir,
        Merger $merger,
        CssBuilder $cssBuilder,
        CssMinifier $cssMinifier
    )
    {
        $this->filesystem = $filesystem;
        $this->flagsDir = $envFlagsDir;
        $this->distDir = $envDistDir;
        $this->merger = $merger;
        $this->cssBuilder = $cssBuilder;
        $this->cssMinifier = $cssMinifier;
    }

    /**
     * @param string[] $countries
     * @param string[] $custom [ $code => $filename ]
     */
    public function generate(
        int $height,
        float $border,
        array $countries,
        array $custom = [],
        $subDir = 'default'
    ): string
    {
        $countries = $countries ?:
            array_map(function ($flagFilename) {
                preg_match('#([^/\\\\]+)\.png#', $flagFilename, $matches);
                return $matches[1];
            }, glob($this->flagsDir . '/*.png'));

        sort($countries);

        $distDir = $this->distDir . '/' . $subDir . '/';

        $zipPath = $distDir . 'avris-flags.zip';

        if ($this->filesystem->exists($zipPath)) {
            return $zipPath;
        }

        $this->filesystem->remove($distDir);
        $this->filesystem->mkdir($distDir);

        $countries = array_merge(
            array_fill_keys($countries, true),
            $custom
        );

        $flagSet = $this->merger->merge($height, $countries, $border);
        $flagSet->getImage()->save($distDir . 'avris-flags.png');

        $css = $this->cssBuilder->build($flagSet, $border);
        $minified = $this->cssMinifier->minify($css);
        file_put_contents($distDir . 'avris-flags.css', $minified);

        file_put_contents($distDir . 'package.json', json_encode(self::NPM_PACKAGE, JSON_PRETTY_PRINT));

        $zip = new \ZipArchive();
        if ($zip->open($zipPath, \ZipArchive::CREATE) !== true) {
            throw new \RuntimeException(sprintf('Cannot create zip archive %s', $zipPath));
        }

        $files = ['avris-flags.css', 'avris-flags.png', 'package.json'];

        foreach ($files as $file) {
            $zip->addFile($distDir . '/' . $file, $file);
        }
        $zip->close();

        return $zipPath;
    }
}
