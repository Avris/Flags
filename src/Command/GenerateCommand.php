<?php
namespace Avris\Flags\Command;

use Avris\Flags\Service\Generator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class GenerateCommand extends Command
{
    /** @var Generator */
    private $generator;

    public function __construct(Generator $generator)
    {
        $this->generator = $generator;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('flags:generate')
            ->addArgument('height', InputArgument::OPTIONAL, 'Ratio is 1.6, default size: 64x102', '64')
            ->addArgument('countries', InputArgument::OPTIONAL, 'Country codes to include, comma separated', '')
            ->addArgument('border', InputArgument::OPTIONAL, 'Transparent border around each flag', '0.05')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $this->generator->generate(
            (int) $input->getArgument('height'),
            (float) $input->getArgument('border'),
            array_filter(explode(',', $input->getArgument('countries')))
        );

        $output->writeln(sprintf('<info>Generated to "%s"</info>', $path));
    }
}
