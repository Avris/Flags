<?php
namespace Avris\Flags\Command;

use Avris\Flags\Model\Countries;
use Avris\Flags\Service\ScraperInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ScrapeCommand extends Command
{
    /** @var ScraperInterface */
    private $scraper;

    public function __construct(ScraperInterface $scraper)
    {
        $this->scraper = $scraper;
        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setName('flags:scrape')
            ->addArgument('height', InputArgument::OPTIONAL, 'Ratio is 1.6, default size: 64x102', '64')
            ->addArgument('countries', InputArgument::OPTIONAL, 'Country codes to include', '')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $countries = $input->getArgument('countries');
        $countries = $countries
            ? array_intersect_key(Countries::$list, array_flip(explode(',', $countries)))
            : Countries::$list;

        $messages = $this->scraper->scrape(2 * $input->getArgument('height'), $countries);
        foreach ($messages as $message) {
            $output->writeln($message);
        }
    }
}
